import threading
import time

from config import Config
import telebot
from telebot import types
from bot_methods import BotBase
import logging
from sqlite_controller import Sqlite_controller as Db
import flask
import logging
import multiprocessing

# Sometimes have to write full path
DBNAME = 'base.db'
LOGNAME = 'logs.log'

# Инициализация переменных бд и бота
bot = telebot.TeleBot(Config.token)
botBase = BotBase(bot, Config.qiwi_token, Config.qiwi_number, dbname=DBNAME, logname=LOGNAME)

# Установка логов
telebot.logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(LOGNAME)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
telebot.logger.addHandler(fh)

logger = BotBase.logger_init(LOGNAME, 'main')

# создание экземпляра фласка, для вебхуков
app = flask.Flask(__name__)


def keyboard_roles():
    keyboard = types.ReplyKeyboardMarkup(True, False)
    keyboard.row('Рекламодатель', 'Админ')
    return keyboard


def keyboard_main():
    keyboard = types.ReplyKeyboardMarkup(True, False)
    keyboard.row('Создать заявку', 'Смотреть сделки')
    return keyboard


def keyboard_back_to_start():
    keyboard = types.ReplyKeyboardMarkup(True, False)
    keyboard.row('Назад')
    keyboard.row('Отмена')
    return keyboard


def keyboard_conditions():
    keyboard = types.ReplyKeyboardMarkup(True, False)
    keyboard.row(botBase.conditions[0], botBase.conditions[1])
    keyboard.row(botBase.conditions[2], botBase.conditions[3])
    keyboard.row(botBase.conditions[4], botBase.conditions[5])
    return keyboard


def keyboard_money_send(number, deal_id):
    """
    Prepeare data for send money to admin
    :param number: <str> qiwi phone number
    :param deal_id: <int> db deal id
    :return: <telebot.types.keyboard> prepared keyboard
    """
    keyboard = types.InlineKeyboardMarkup()
    callback_button = types.InlineKeyboardButton(text="Отправить",
                                                 callback_data="Send %s %s" % (str(number), str(deal_id)))
    keyboard.add(callback_button)

    return keyboard


def keyboard_reject_order(deal_id):
    """
    Prepeare data for cancel adv order from the admin side
    :param deal_id: <int> db deal id
    :return: <telebot.types.keyboard> prepared keyboard
    """
    keyboard = types.InlineKeyboardMarkup()
    callback_button = types.InlineKeyboardButton(text="Отклонить",
                                                 callback_data="Reject %s" % str(deal_id))
    keyboard.add(callback_button)

    return keyboard


@bot.message_handler(func=lambda message: message.text == '/start', content_types=['text'])
def start(message):
    botBase.create_user(message.chat.id, 'start', message.chat.username)
    try:
        logger.debug(message.chat.username + ' called /start')
    except:
        logger.debug(message.chat.id + ' called /start')
    bot.send_message(message.chat.id, 'Привет, ' + str(message.chat.username) + '?',
                     reply_markup=keyboard_main())


@bot.message_handler(func=lambda message: message.text == 'Назад')
def back(message):
    unfinished_deal = botBase.unfinished_deal(message.chat.id)
    if unfinished_deal is not None:
        botBase.set_previous_state(unfinished_deal)
        bot.send_message(message.chat.id, 'Данные ввода удалены, пожалуйста повторите ввод',
                         reply_markup=keyboard_back_to_start())
    # TODO: вывод сообщения о том, на какое состояние вернулся пользователь


"""
@bot.message_handler(func=lambda message: message.text == 'Создать заявку', content_types=['text'])
def start(message):
    botBase.add_user(message.chat.id, message.chat.username)

    bot.send_message(message.chat.id, 'Как ты хочешь создать заявку ?', reply_markup=keyboard_choose_type())
"""


@bot.message_handler(func=lambda message: message.text == 'Создать заявку', content_types=['text'])
def create_order(message):
    unfinished_deal = botBase.unfinished_deal(message.chat.id)

    if unfinished_deal is not None:
        # если у человека есть незавершённые сделки, то выводим ему информацию о том, с кем она
        if unfinished_deal['admin_id'] == message.chat.id:
            bot.send_message(message.chat.id,
                             'У тебя есть незаполненная сделка с ' +
                             str(botBase.get_username(unfinished_deal['advertiser_id'])) + ' на канале ' +
                             str(unfinished_deal['channel']),
                             reply_markup=keyboard_back_to_start())
        else:
            bot.send_message(message.chat.id,
                             'У тебя есть незаполненная сделка с ' + str(botBase.get_username(
                                 unfinished_deal['admin_id'])) + ' на канале ' +
                             str(unfinished_deal['channel']),
                             reply_markup=keyboard_back_to_start())
    else:
        bot.send_message(message.chat.id, 'Введи никнейм админа',
                         reply_markup=keyboard_back_to_start())
        botBase.deal_create(message.chat.id, 'advertiser')


@bot.message_handler(func=lambda message: message.text == 'Готово', content_types=['text'])
def done(message):
    bot.send_message(message.chat.id, 'Отлично, можешь вернуться',
                     reply_markup=keyboard_back_to_start())


@bot.message_handler(func=lambda message: message.text == 'Смотреть сделки', content_types=['text'])
def see_orders(message):
    deals = botBase.user_deals(message.chat.id)
    user_id = message.chat.id

    if deals is not None:
        for deal in deals:
            if deal['admin_id'] == user_id:
                bot.send_message(message.chat.id, 'сделка с ' + botBase.get_username(deal['advertiser_id']) + '\n' +
                                 'id сделки: ' + str(deal['id']) + '\n' +
                                 'на канале ' + str(deal['channel']) + '\n' +
                                 'на сумму ' + str(deal['price']) + ' рублей\n' +
                                 'статус: ' + str(deal['state'])
                                 , reply_markup=keyboard_main())
            else:
                bot.send_message(message.chat.id, 'сделка с ' + botBase.get_username(deal['admin_id']) + '\n' +
                                 'id сделки: ' + str(deal['id']) + '\n' +
                                 'на канале ' + str(deal['channel']) + '\n' +
                                 ' на сумму ' + str(deal['price']) + ' рублей\n' +
                                 'статус: ' + str(deal['state'])
                                 , reply_markup=keyboard_main())

    else:
        bot.send_message(message.chat.id, ' У тебя нет сделок', reply_markup=keyboard_main())


@bot.message_handler(func=lambda message: message.text == 'Проверить', content_types=['text'])
def order_validate(message):
    unfinished_deal = botBase.unfinished_deal(message.chat.id)
    if unfinished_deal is not None:
        transaction = botBase.qiwi_find_transaction(unfinished_deal['id'])
        if transaction is not None:

            # изменяем значения сделки и заносим в бд (первые 3 строки для наглядности)
            unfinished_deal['price'] = transaction['sum']
            unfinished_deal['state'] = 'payed'
            unfinished_deal['AdvertiserPersonId'] = transaction['personId']
            botBase.deal_update(unfinished_deal['id'], unfinished_deal['price'], unfinished_deal['state'],
                                unfinished_deal['AdvertiserPersonId'])

            bot.send_message(message.chat.id, 'Мне пришло ' + str(transaction['sum']) +
                             ' рублей. Сейчас я скину администратору рекламный текст и условия и буду тщательно наблюдать за их исполнением.\n'
                             'Если ты напутал в сделке и хочешь её отменить (и само собой вернуть деньги) пропиши команду /reject',
                             reply_markup=keyboard_main())

            admin = unfinished_deal['admin_id']

            logger.debug('Unfinished deal: ' + str(unfinished_deal))

            bot.send_message(admin,
                             botBase.get_username(unfinished_deal['advertiser_id']) + ' скинул мне ' + str(
                                 unfinished_deal['price']) +
                             ' рублей. После того как ты выполнишь условия рекламы ты получишь ' + str(
                                 unfinished_deal['price'] - unfinished_deal['price'] * botBase.PERCENT),
                             reply_markup=keyboard_main())
            bot.send_message(admin, 'Условия: ' + unfinished_deal['condition'] +
                             ' Помни, что рекламный текст должен быть выложен в НЕИЗМЕННОМ виде, иначе шекелей тебе не видать, у нас с этим строго.'
                             ' Как сделаешь, скинь мне ссылку на выложенный пост или просто репостни запись',
                             reply_markup=keyboard_main())
            file_id = unfinished_deal['file_id']
            # TODO: проверка файла (не по id, т.к пикчу могут попросту скачать)
            # Клавиатура для отклонения заявки админом
            keyboard_reject = keyboard_reject_order(unfinished_deal['id'])
            if file_id is not None:
                bot.send_photo(admin, file_id, caption=unfinished_deal['text'], reply_markup=keyboard_reject)
            else:
                bot.send_message(admin, unfinished_deal['text'], reply_markup=keyboard_reject)

        else:
            bot.send_message(message.chat.id, 'Шекели ещё не поступили',
                             reply_markup=types.ReplyKeyboardMarkup(True, False).row('Проверить'))
    else:
        bot.send_message(message.chat.id, 'У тебя нет сделок, которые тебе нужно оплатить',
                         reply_markup=keyboard_main())


@bot.message_handler(func=lambda message: message.text == '/cancel' or message.text == 'Отмена', content_types=['text'])
def cancel(message):
    unfinished_deal = botBase.unfinished_deal(message.chat.id)

    logger.debug('unfinished deal canceled: ' + str(unfinished_deal))

    if unfinished_deal is not None and (unfinished_deal['state'] != 'payed'
                                        or unfinished_deal['state'] != 'finished'
                                        or unfinished_deal['state'] != 'success'):
        botBase.deal_remove(unfinished_deal['id'])

        bot.send_message(message.chat.id, 'Твоя незаконченная заявка отменена, можешь создать другую',
                         reply_markup=keyboard_main())
    else:
        bot.send_message(message.chat.id, 'У тебя нет незаконченных заявок',
                         reply_markup=keyboard_main())


@bot.message_handler(commands=['reject'])
def reject(message):
    deals = botBase.deal_find_with_state(message.chat.id, 'payed', fetch_list=True, admin_role=False)
    if deals is None:
        bot.send_message(message.chat.id, 'У тебя нет оплаченных заявок, которые можно отменить')
        return
    for deal in deals:
        try:
            botBase.qiwi_send_money(deal['AdvertiserPersonId'],
                                    (deal['price'] - deal['price'] * botBase.ERR_PERCENT),
                                    'Сделка с ' + botBase.get_username(deal['admin_id']) + ' отменена')
        except botBase.QiwiException:
            logger.error('qiwi failed to send money for deal: ' + str(deal))
            bot.send_message(deal['advertiser_id'], ' Не удалось вернуть деньги')
            return

        bot.send_message(deal['admin_id'],
                         botBase.get_username(deal['advertiser_id']) + ' отменил сделку по рекламе, отбой',
                         reply_markup=keyboard_main())
        bot.send_message(deal['advertiser_id'],
                         'сделка с ' + botBase.get_username(deal['admin_id']) + ' отменена',
                         reply_markup=keyboard_main())

        botBase.deal_update_state(deal['id'], 'failed')


@bot.callback_query_handler(func=lambda call: call.data[:4] == "Send")
def call_back(call):
    data = call.data.split(" ")
    deal_id = data[2]
    logger.debug("callback function called with params:" + str(data))
    phone_number = data[1]
    deals = botBase.deal_find_with_state(call.message.chat.id, 'posted', fetch_list=True)
    for deal in deals:
        if deal['id'] == int(deal_id):
            botBase.deal_change_param(deal['id'], 'AdminPersonId', phone_number)
            try:
                botBase.qiwi_send_money(phone_number, (deal['price'] - deal['price'] * botBase.PERCENT),
                                        "За сделку " + str(deal['id']))
            except botBase.QiwiException:
                bot.send_message(call.message.chat.id, 'Не удалось отправить деньги')
                return

            botBase.deal_update_state(deal['id'], 'success')
            bot.send_message(call.message.chat.id, 'Деньги переведены на твой кошелёк, лови',
                             reply_markup=keyboard_main())
            bot.send_message(deal['advertiser_id'],
                             'Деньги за рекламу поста на канале ' + deal['channel'] + ' успешно переведены')


@bot.callback_query_handler(func=lambda call: call.data[:6] == "Reject")
def call_back(call):
    data = call.data.split(" ")
    deal_id = data[1]
    logger.debug("callback function called with params:" + str(data))
    deal = botBase.deal_get(deal_id)
    if deal['state'] == 'payed' and deal['admin_id'] == call.message.chat.id:
        try:
            botBase.qiwi_send_money(deal['AdvertiserPersonId'],
                                    (deal['price'] - deal['price'] * botBase.ERR_PERCENT),
                                    'Сделка с ' + botBase.get_username(deal['admin_id']) + ' отменена')
        except botBase.QiwiException:
            logger.error('qiwi failed to send money for deal: ' + str(deal))
            bot.send_message(deal['advertiser_id'],
                             ' Не удалось вернуть деньги за сделку с ' + botBase.get_username(deal['admin_id']))
            bot.send_message(deal['admin_id'],
                             ' Возникли трудности при отмене сделки  с ' + botBase.get_username(deal['advertiser_id']))
            return

        bot.send_message(deal['admin_id'],
                         botBase.get_username(deal['advertiser_id']) + ' сделка ' + str(deal['id']) + ' отменена',
                         reply_markup=keyboard_main())
        bot.send_message(deal['advertiser_id'],
                         botBase.get_username(deal['admin_id']) + ' отклонил сделку ' + str(deal['id']),
                         reply_markup=keyboard_main())

        botBase.deal_update_state(deal['id'], 'failed')
        return

    bot.send_message(call.message.chat.id, 'Данную сделку уже невозможно отменить')


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id,
                     'По вопросам и трудностям (нам действительно интересно у кого могут возникнуть трудности в взаимодействии) писать @sensiarion, @greatdeveloper\n'
                     'Комиссии: ' + str(botBase.PERCENT * 100) + '% при успешной сделке, ' + str(
                         botBase.ERR_PERCENT * 100) + '% при нарушении условий\n'
                                                      'Доступные варинаты оплаты: qiwi',
                     reply_markup=keyboard_main())


# TODO: all content_types=['text'] message handler, which will be check message with depend on user's state
@bot.message_handler(content_types=['text'])
def text_handle(message):
    """
    states = {
        'start',
        'nickname',
        'channel',
        'text',
        'condition',
        'finished',
        'payed',
        'validate',
        'posted'
        'success',
        'failed'
    }
    """
    posted_deal = botBase.deal_find_with_state(message.chat.id, 'posted')
    if posted_deal is not None:
        bot.send_message(message.chat.id, ' Ваш номер: ' + message.text,
                         reply_markup=keyboard_money_send(message.text, posted_deal['id']))

        return

    payed_deal = botBase.deal_find_with_state(message.chat.id, 'payed')
    if payed_deal is not None:
        url = botBase.check_post(message, payed_deal['text'], payed_deal['channel'])
        if url is not None:
            payed_deal['state'] = 'validate'
            payed_deal['timestamp'] = int(time.time())
            payed_deal['url'] = url
            botBase.payed_deal_update(payed_deal['id'], payed_deal['state'], int(time.time()), payed_deal['url'])
            bot.send_message(message.chat.id,
                             'Прекрасно, это действительно пост из группы. Я прослежу за тем, чтобы ты соблюдал условия (могу опоздать примерно на 5 минут, я бот и мне позволительно) и потребую твой qiwi кошелек)',
                             reply_markup=keyboard_main())

            return
        else:
            bot.send_message(message.chat.id,
                             'Я не нашёл пост по твоей ссылке или же текст не соответствует оговоренному, ссылка должна выглядеть таким образом: https://t.me/your_channel/100',
                             reply_markup=keyboard_main())
            return

    unfinished_deal = botBase.unfinished_deal(message.chat.id)
    logger.debug('Starting to handle deal state for unf deal: ' + str(unfinished_deal))
    if unfinished_deal is not None:
        if unfinished_deal['state'] == 'start':
            # считываем ник
            # """
            if botBase.user_find(message.text) is None:
                bot.send_message(message.chat.id,
                                 'Этот человек ещё не писал мне, скажи ему постучаться мне командой /start',
                                 reply_markup=keyboard_back_to_start())
                return
            # """
            unfinished_deal['admin'] = message.text
            unfinished_deal['state'] = 'nickname'
            # TODO one func with change any param and state
            botBase.deal_change_param(unfinished_deal['id'], 'admin_id', botBase.user_find(message.text))
            botBase.deal_update_state(unfinished_deal['id'], 'nickname')
            bot.send_message(message.chat.id,
                             'Теперь введи название канала (без @ и не в формате ссылки, а лишь название)',
                             reply_markup=keyboard_back_to_start())
            return

        if unfinished_deal['state'] == 'nickname':
            # считываем канал
            if not botBase.check_channel(message.text):
                bot.send_message(message.chat.id, 'Я не нашёл этот канал в телеграме!',
                                 reply_markup=keyboard_back_to_start())
                return
            unfinished_deal['channel'] = message.text
            unfinished_deal['state'] = 'channel'
            botBase.deal_change_param(unfinished_deal['id'], 'channel', message.text)
            botBase.deal_update_state(unfinished_deal['id'], 'channel')
            bot.send_message(message.chat.id,
                             'Теперь введи рекламный текст, если хочешь скинуть ещё и изображение, то кидай его перед тем как отправлять текст',
                             reply_markup=keyboard_back_to_start())
            return

        if unfinished_deal['state'] == 'channel':
            # считываем текст
            unfinished_deal['text'] = message.text
            unfinished_deal['state'] = 'text'
            botBase.deal_change_param(unfinished_deal['id'], 'text', message.text)
            botBase.deal_update_state(unfinished_deal['id'], 'text')
            bot.send_message(message.chat.id, 'А теперь условие выкладки',
                             reply_markup=keyboard_conditions())
            return

        if unfinished_deal['state'] == 'text':
            if not botBase.condition_find(message.text):
                bot.send_message(message.chat.id, 'Я не знаю подобных условий размещения',
                                 reply_markup=keyboard_conditions())
                return
            unfinished_deal['condition'] = message.text
            unfinished_deal['state'] = 'condition'
            botBase.deal_change_param(unfinished_deal['id'], 'condition', message.text)
            botBase.deal_update_state(unfinished_deal['id'], 'condition')

            bot.send_message(message.chat.id, 'Отлично! теперь тебе необходимо занести шекелей на номер +' + str(
                botBase.phone_number) + ' с комментарием: ' + str(unfinished_deal['id']),
                             reply_markup=types.ReplyKeyboardMarkup(True, False).row('Проверить'))

            return
        try:
            logger.debug('Text handle bring to no deal state for user' + message.chat.username)
        except:
            logger.debug('Text handle bring to no deal state for user' + message.chat.id)


@bot.message_handler(content_types=['photo'])
def file_handler(message):
    unfinished_deal = botBase.unfinished_deal(message.chat.id)
    if unfinished_deal['state'] == 'channel':
        photo = message.json.get('photo')
        qual = len(message.json.get('photo'))
        unfinished_deal['file_id'] = photo[qual - 1].get('file_id')
        botBase.deal_change_param(unfinished_deal['id'], 'file_id', photo[qual - 1].get('file_id'))
        logger.debug('Photo added to deal: ' + str(unfinished_deal['id']))


def money_sender(deals):
    """
    Get and check deals with 'validate' state and verifying the conditions
    if conditions is done, change state and ask admin for a wallet
    :param deals:
    :return:
    """
    m_logger = BotBase.logger_init(LOGNAME, 'main_money_sender')
    m_logger.info('Validate started')
    SLEEP_TIME = 300

    while True:
        m_logger.info('loop iteration ended')
        deals = botBase.deals_get_with_state('validate')
        if deals is None or len(deals) == 0:
            time.sleep(SLEEP_TIME)
            continue
        for deal in deals:

            logging.debug(deal)

            post_check = botBase.check_post(deal['url'], deal['text'], deal['channel'])
            if int(time.time()) - deal['timemark'] < 3600 * int(deal['condition'][0]):
                # проверка на то существует ли пост
                if post_check is None:
                    bot.send_message(deal['admin_id'],
                                     'Ты нарушил условия по рекламе (удалил или изменил пост раньше оговоренного срока) и денег не получишь !')
                    bot.send_message(deal['advertiser_id'],
                                     'Администратор группы нарушил условия рекламы, деньги возвращены тебе на кошелёк за вычетом 1% (мы тоже старались, приятель)')
                    botBase.qiwi_send_money(deal['AdvertiserPersonId'],
                                            deal['price'] - deal['price'] * botBase.ERR_PERCENT,
                                            'Деньги за неудавшуюся сделку с ' + botBase.get_username(
                                                deal['admin_id']))
                    deal['state'] = 'failed'
                    botBase.deal_update_state(deal['id'], 'failed')
                    continue
                # проверка на то в топе ли пост
                if botBase.next_post_check(deal['url']):
                    bot.send_message(deal['admin_id'],
                                     'Ты нарушил условия по рекламе (выложил следующий пост раньше оговоренного срока) и денег не получишь !',
                                     reply_markup=keyboard_main())
                    bot.send_message(deal['advertiser_id'],
                                     'Администратор группы нарушил условия рекламы, деньги возвращены тебе на кошелёк за вычетом 1% (мы тоже старались, приятель)',
                                     reply_markup=keyboard_main())
                    botBase.qiwi_send_money(deal['AdvertiserPersonId'],
                                            deal['price'] - deal['price'] * botBase.ERR_PERCENT,
                                            'Деньги за неудавшуюся сделку с ' + botBase.get_username(
                                                deal['admin_id']))
                    deal['state'] = 'failed'
                    botBase.deal_update_state(deal['id'], 'failed')
                    continue

            if int(time.time()) - deal['timemark'] <= 3600 * int(deal['condition'][2:]):
                if post_check is None:
                    bot.send_message(deal['admin_id'],
                                     'Ты нарушил условия по рекламе (удалил или изменил пост раньше оговоренного срока) и денег не получишь !',
                                     reply_markup=keyboard_main())
                    bot.send_message(deal['advertiser_id'],
                                     'Администратор группы нарушил условия рекламы, деньги возвращены тебе на кошелёк за вычетом 1% (мы тоже старались, приятель)',
                                     reply_markup=keyboard_main())
                    botBase.qiwi_send_money(deal['AdvertiserPersonId'],
                                            deal['price'] - deal['price'] * botBase.ERR_PERCENT,
                                            'Деньги за неудавшуюся сделку с ' + botBase.get_username(
                                                deal['admin_id']))
                    deal['state'] = 'failed'
                    botBase.deal_update_state(deal['id'], 'failed')
                    continue

            else:
                bot.send_message(deal['admin_id'],
                                 'Молодец, ты выполнил все условия по сделке, теперь отправь мне номер своего qiwi кошелька (в формате: 79999999999), что бы я мог отправить тебе твои деньги',
                                 reply_markup=keyboard_main())
                deal['state'] = 'posted'
                botBase.deal_update_state(deal['id'], 'posted')

        m_logger.info('loop iteration ended')
        time.sleep(SLEEP_TIME)


@app.route('/', methods=['GET', 'HEAD'])
def index():
    return ''


@app.route('/' + Config.token + '/', methods=['POST'])
def webhook():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        logger.debug('updates from webhook: ' + str(update))
        return ''
    else:
        flask.abort(403)


if __name__ == "__main__":
    bot.remove_webhook()
    time.sleep(1)
    bot.set_webhook(url='https://%s:%s/%s/' % (Config.webhook_ip, Config.webhook_port, Config.token),
                    certificate=open(Config.ssl_cert, 'r'))
    validate_check = multiprocessing.Process(target=money_sender, args=(botBase.deals_get(),))
    validate_check.daemon = True
    validate_check.start()

    app.run(host='0.0.0.0', port=Config.webhook_port, ssl_context=(Config.ssl_cert, Config.ssl_pom))

    logger.info('bot started')
    # bot.polling(none_stop=True, interval=0)
