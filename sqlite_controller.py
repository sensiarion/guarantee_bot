import sqlite3
import bot_methods


class Sqlite_controller:
    q = {
        'users_create': "CREATE TABLE IF NOT EXISTS users "
                        "(chat_id INTEGER PRIMARY KEY NOT NULL, "
                        "username VARCHAR(128), "
                        "state VARCHAR(64) NOT NULL);",

        'deals_create': "CREATE TABLE IF NOT EXISTS deals "
                        "(id INTEGER PRIMARY KEY NOT NULL,"
                        "advertiser_id INTEGER,"
                        "admin_id INTEGER,"
                        "maintainer_id INTEGER,"
                        "channel VARCHAR(128), "
                        "price INTEGER,"
                        "condition VARCHAR(64),"
                        "state VARCHAR(64),"
                        "text VARCHAR(4096),"
                        "url VARCHAR(1024),"
                        "file_id INTEGER,"
                        "timemark INTEGER,"
                        "AdvertiserPersonId VARCHAR(64),"
                        "AdminPersonId VARCHAR(64));",

    }

    """
        ПРИМЕЧАНИЕ: если вы добавляете в свою логику любое состояние после 'payed',
         то его необходимо добавлять в функцию unfinished_deals() в качестве исключения

        этапы сделок:
            start - сделка создана и записан никнейм создавшего
            nickname - введён никнейм второго участника
            channel - введён ли канал 
            text - введён рекламный текст
            condition - введено условие выкладки  
            finished - закончен ввод начальных данных
            payed - деньги переведны боту
            validate - данные передены, ожидается выкладка поста админом 
            posted - админ уже выложил пост
            success - деньги выплачены и сделка завершена
            failed - условия не были выполнены
    """

    states = {
        0: 'start',
        1: 'nickname',
        2: 'channel',
        3: 'text',
        4: 'condition',
        5: 'finished',
        6: 'payed',
        7: 'validate',
        8: 'posted',
        9: 'success',
        10: 'failed',
    }

    def state_validate(self, state):
        for value in self.states.values():
            if state == value:
                return True

        raise WrongUserStateException

    # Я знаю, что очень не хорошо писать функциии перед конструктором класса, но это всё в угоду логичности блоков кода (да, я слишком ленив, что бы красиво разбить это на файлы)

    def __init__(self, dbname, logname='logs.log'):
        self.DBNAME = dbname
        self.logger = bot_methods.BotBase.logger_init(logname, 'sqlite_controller')
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(self.q['users_create'])
            connection.execute(self.q['deals_create'])
            connection.commit()

    def get_db_connection(self):
        """
        :param dbname: <str> name of database (like test.db)
        :return: <sqlite3.connection> object to communicate with db
        """
        self.logger.debug('database connection called :' + self.DBNAME)
        return sqlite3.connect(self.DBNAME)

    def fetch_k_v(self, cursor, fetch_list=False):
        """
        pack fetched from db values with their keys
        :param cursor <sqlite3.cursor> cursor with executed SELECT query
        :return: <dict,None> key: value dict with values and column names, None if returned empty
        """
        values = cursor.fetchall()
        description = cursor.description
        keys = []
        answer = []
        if values is None or len(values) == 0:
            return None
        for i in description:
            keys.append(i[0])
        for unit in values:
            answer.append(dict(zip(keys, unit)))
        if fetch_list:
            return answer
        if len(answer) == 1:
            return answer[0]
        return answer

    def create_user(self, chat_id, state, username=None):
        """
        :param chat_id: <int> message.chat.id
        :param state: <str> user's state
        :param username: <str> nickname
        :return: None
        """
        self.state_validate(state)
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT chat_id FROM users WHERE chat_id = ?", (chat_id,))
            if cursor.fetchone() is None:
                connection.execute("INSERT INTO users (chat_id,username,state) VALUES (?,?,?)",
                                   (chat_id, username, state))
                connection.commit()
                cursor.close()

    def get_user(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return: full user's info from DB
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM users WHERE chat_id = ?", (chat_id,))
            response = cursor.fetchone()
            cursor.close()
            return response

    def get_username(self, chat_id):
        """
        :param chat_id: <int> message.chat.id
        :return: <str> username
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT username FROM users WHERE chat_id = ?", (chat_id,))
            response = cursor.fetchone()
            if response is None:
                return str(chat_id)
            cursor.close()
            return response[0]

    def user_state_update(self, chat_id, new_state):
        """
        update user's state in DB
        :param chat_id: <int> message.chat.id
        :param new_state: <str>
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute("UPDATE users SET state = ? WHERE chat_id = ?", (new_state, chat_id))
            connection.commit()

    def user_find(self, username):
        """
        :param username: <str> person's tg username
        :return: <None/int> chat_id if exists, None else
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT chat_id FROM users WHERE username = ?", (username,))
            fetch = cursor.fetchone()
            if fetch is None:
                return None
            chat_id = fetch[0]
            self.logger.debug('user_find  CHAT_ID ' + str(chat_id))
            cursor.close()
            return chat_id

    def user_update_param(self, chat_id, param, value):
        """
        :param chat_id: <int> message.chat.id
        :param param: <str> user's table param (state,do_send,etc.)
        :param value: <>
        :return: None
        """
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(("UPDATE users SET " + param + " = ? WHERE chat_id = ?"), (value, chat_id))
            connection.commit()

    def user_update_params(self, chat_id, params, values):
        """
        :param chat_id: <int> message.chat.id
        :param params: <list>
        :param values: <list>
        :return: None
        """
        s = "UPDATE users SET "
        if len(params) != len(params):
            raise KeyError
        if type(params) != list or type(values) != list:
            raise TypeError

        for i in range(len(params)):
            s += str(params[i]) + '=?,'
        s = s[:-1] + " WHERE chat_id = ?"

        values.append(chat_id)
        with sqlite3.connect(self.DBNAME) as connection:
            connection.execute(s, values)
            connection.commit()

    def user_get_param(self, chat_id, param):
        """
        :param chat_id: <int> message.chat.id
        :param param: <str> name of param
        :return: <any> value of choosed param
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT " + param + " FROM users WHERE chat_id = ?", (chat_id,))
            response = cursor.fetchone()[0]
            cursor.close()
            return response

    def get_users(self):
        """
        :return: <tuple> all users from DB
        """
        with sqlite3.connect(self.DBNAME) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM users")
            return cursor.fetchall()

    def deal_create(self, maintainer_id, role='advertiser'):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT max(id) FROM deals")
            fetch = cursor.fetchone()[0]
            if fetch is None:
                fetch = 0
            deal_id = fetch + 1
            if role == 'advertiser':
                connection.execute("INSERT INTO deals (id,advertiser_id,maintainer_id,state) VALUES (?,?,?,?)",
                                   (deal_id,
                                    maintainer_id, maintainer_id, 'start'))
            else:
                connection.execute("INSERT INTO deals (id,admin_id,maintainer_id,state) VALUES (?,?,?,?)", (deal_id,
                                                                                                            maintainer_id,
                                                                                                            maintainer_id,
                                                                                                            'start'))
            connection.commit()
        return deal_id

    def deal_find(self, deal_id):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT* FROM deals WHERE id = ?", (deal_id,))
            ans = cursor.fetchone()
            cursor.close()
            if ans[0] is None:
                return False
            else:
                return True

    def deals_get(self):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM deals")
            ans = self.fetch_k_v(cursor, True)
            self.logger.debug("deals_get fetch: " + str(ans))
            cursor.close()
            return ans

    def deals_get_with_state(self, state):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM deals WHERE state = ?", (state,))
            ans = self.fetch_k_v(cursor, True)
            self.logger.debug("deals_get with state fetch: " + str(ans))
            cursor.close()
            return ans

    def deal_get(self, deal_id):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM deals WHERE id = ?", (deal_id,))
            ans = self.fetch_k_v(cursor)
            self.logger.debug("deal_get fetch: " + str(ans))
            cursor.close()
            return ans

    def deal_update_state(self, deal_id, state):
        self.state_validate(state)
        with self.get_db_connection() as connection:
            self.logger.debug("deal_update state " + state + "  - - " + str(deal_id))
            connection.execute("UPDATE deals SET state = ? WHERE id = ?", (state, deal_id))
            connection.commit()

    def deal_change_param(self, deal_id, param, value):
        with sqlite3.connect(self.DBNAME) as connection:
            query = "UPDATE deals SET " + str(param) + " = ? WHERE id = ?"
            connection.execute(query, (value, deal_id))
            connection.commit()

    def deal_update(self, deal_id, price, state, adviser_person_id):
        with self.get_db_connection() as connection:
            connection.execute("UPDATE deals SET price = ?,state = ?,AdvertiserPersonId = ?  WHERE id = ?",
                               (price, state, adviser_person_id, deal_id))
            connection.commit()

    def payed_deal_update(self, deal_id, state, timemark, url):
        with self.get_db_connection() as connection:
            connection.execute("UPDATE deals SET state = ? ,timemark = ?,url = ?  WHERE id = ?",
                               (state, timemark, url, deal_id))
            connection.commit()

    def deal_remove(self, deal_id):
        with self.get_db_connection() as connection:
            connection.execute("DELETE FROM deals WHERE id =?", (deal_id,))
            connection.commit()

    def user_deals(self, chat_id):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()

            cursor.execute("SELECT * FROM deals WHERE maintainer_id = ?", (chat_id,))
            fetch = self.fetch_k_v(cursor, True)
            cursor.close()
            return fetch

    def unfinished_deal(self, chat_id):
        # TODO test
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            cursor.execute(
                "SELECT * FROM deals WHERE maintainer_id = ? AND state != ? AND state != ? AND state != ? AND state != ? AND state!= ?",
                (chat_id, 'validate', 'posted', 'success', 'payed', 'failed'))

            fetch = self.fetch_k_v(cursor)
            cursor.close()

            self.logger.debug("unfinished deal fetch: " + str(fetch))

            return fetch

    def deal_is_unfinshed(self, deal):

        if deal['state'] != 'validate' and deal['state'] != 'posted' and deal['state'] != 'success' and deal[
            'state'] != 'payed' and deal['state'] != 'failed':
            return True
        return False

    def deal_find_with_state(self, chat_id, state, fetch_list=False, admin_role=True):
        with self.get_db_connection() as connection:
            cursor = connection.cursor()
            if admin_role:
                cursor.execute("SELECT * FROM deals WHERE admin_id = ? AND state = ?", (chat_id, state))
            else:
                cursor.execute("SELECT * FROM deals WHERE advertiser_id = ? AND state = ?", (chat_id, state))
            ans = self.fetch_k_v(cursor, fetch_list)
            cursor.close()
            return ans


class WrongUserStateException(Exception):
    pass


class WrongDealStateException(Exception):
    pass
