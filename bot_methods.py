import json

import requests
import time
from lxml import html
import requests_html
from sqlite_controller import Sqlite_controller
import logging


class BotBase(Sqlite_controller):
    userslist = []
    deals = []
    transactions = []
    PERCENT = 0.05
    ERR_PERCENT = 0.01
    conditions = {
        0: '2/24',
        1: '4/24',
        2: '2/48',
        3: '4/48',
        4: '2/72',
        5: '4/72'
    }

    class QiwiException(Exception):
        pass

    def __init__(self, bot, qiwi_token, phone_number, dbname='base.db', logname='logs.log'):
        super().__init__(dbname, logname)
        self.bot = bot
        self.qiwi_token = qiwi_token
        self.phone_number = phone_number
        self.deals_counter = 0
        self.qiwi_headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.qiwi_token
        }
        self.qiwi_curency = {'rub': '643'}
        self.logger = self.logger_init(logname, 'bot_methods')

    def condition_find(self, value):
        """
        Check existing of writed condition

        :param value:
        :return:
        """
        for tup in self.conditions.items():
            if tup[1] == value:
                return True
        return False

    def set_previous_state(self, deal):
        """
        For 'Back' button
        NEED TO CHECK DOES IT unfinished or not
        :param deal:
        :return:
        """

        if deal['state'] == 'start':
            return False
        if super().deal_is_unfinshed(deal):
            for tup in self.states.items():
                if tup[1] == deal['state']:
                    previous_state = self.states.get(tup[0] - 1)
                    super().deal_update_state(deal['id'], previous_state)
                    return True

        return False

    @staticmethod
    def check_channel(channel_name):
        """

        :param channel_name: <str> like (cryptogovno) WITHOUT '@' or link chars, etc.
        :return: True if exists
        """
        response = requests.get(url='https://t.me/' + channel_name)
        tree = html.fromstring(response.text).xpath('//a[@ class = "tgme_action_button_new"]')
        try:
            if tree[0].text == 'View Channel':
                return True
        except IndexError:
            return False

        return False

    @staticmethod
    def next_post_check(url):
        """
        Check existing of next post after the arg:url post
        :param url: <str> Url to post
        :return: <boolean> True if exists, False if not
        """

        tokens = url.split('/')
        next_post_number = int(tokens[len(tokens) - 1])
        tokens[len(tokens) - 1] = str(next_post_number + 1)
        next_url = ''
        for token in tokens:
            next_url += '/' + token
        next_url = next_url[1:]
        url = url + '?embed=1'
        next_url = next_url + '?embed=1'

        session = requests_html.HTMLSession()

        r = session.get(next_url)

        try:
            next_url_text = r.html.find('.tgme_widget_message_text')[0].text
        except IndexError:
            return False

        return True

    @staticmethod
    def url_validate(url, channel_name):
        try:
            tokens = url.split('/')
            if channel_name == tokens[3]:
                return True
            return False
        except:
            return False

    @staticmethod
    def check_post(message, text, channel):
        """
        check post existing and containing the necessary arg:text
        :param channel: <str>
        :param text: <str>
        :param message: <telebot.Message>
        :return: <string/None> string url if post is fine, None else
        """
        try:
            if message.forward_from_chat.type == 'channel' and message.forward_from_chat.username == channel:
                if hash(message.text) == hash(text):
                    return 'https://t.me/' + message.forward_from_chat.username + '/' + str(
                        message.forward_from_message_id)
        except AttributeError:
            pass

        try:
            if type(message) == str:
                message_text = message
            else:
                message_text = message.text
            if BotBase.url_validate(message_text, channel):
                response = requests.get(message_text)
                url_text = html.fromstring(response.text).xpath('//meta[@ property ="og:description"]')[0].get(
                    'content')

                if hash(url_text) == hash(text):
                    return message_text
                else:
                    return None
            else:
                return None
        except:
            return None

    def qiwi_ping(self):
        """

        :return: <float/boolean> balance count or False if request failed
        """
        url = "https://edge.qiwi.com/funding-sources/v2/persons/" + self.phone_number + "/accounts"
        response = requests.get(url=url, headers=self.qiwi_headers)

        if response.status_code == 200:
            self.logger.debug("Balance: " + str(response.json()['accounts'][0]['balance']['amount']) + " RUB")
            return response.json()['accounts'][0]['balance']['amount']

        self.logger.error('Usage: qiwi_ping'
                          'Failed to do simple request.Are the token and number correct?\n status code: ' + str(
            response.status_code) + '\nresponse text: ' + response.text)
        return False

    def qiwi_payments(self, count, operation='ALL'):
        """

        :param count: <int> count of transactions will returned (if this count exists on wallet)
                count<50
        :param operation: <string> type of transaction (IN,OUT,ALL)
        :return: <list/boolean> list of tuple with SUCCESS transaction or False if failed to do request
        """
        url = "https://edge.qiwi.com/payment-history/v2/persons/" + self.phone_number + "/payments?"
        params = {
            'rows': count,
            'operation': operation
        }

        response = requests.get(url=url, params=params, headers=self.qiwi_headers)

        if response.status_code == 200:
            transactions = []
            for pay in response.json()['data']:
                if pay['statusText'] == 'Success':
                    transactions.append({'txnId': pay['txnId'],
                                         'personId': str(pay['account']),
                                         'sum': pay['sum']['amount'],
                                         'status': pay['statusText'],
                                         'comment': pay['comment'],
                                         'date': pay['date']})

            if len(transactions) == 0:
                self.logger.error('Usage: qiwi_payments\n'
                                  'No transactions was returned.')
            return transactions

        self.logger.error('Usage: qiwi_payments\n'
                          'Failed to execute request. It is alright ?\n status code: ' + str(
            response.status_code) + '\n response text: ' + response.text)
        return False

    def qiwi_transaction_info(self, transaction_id):
        """

        :param transaction_id: <int> number of transaction
        :return: <tuple/boolean> False if failed to do request or tuple with info
        """
        url = "https://edge.qiwi.com/payment-history/v2/transactions/" + str(transaction_id)

        response = requests.get(url, headers=self.qiwi_headers)
        if response.status_code == 200:
            pay = response.json()
            return {'txnId': pay['txnId'],
                    'personId': str(pay['account']),
                    'type': pay['type'],
                    'sum': pay['sum']['amount'],
                    'status': pay['statusText'],
                    'comment': pay['comment']
                    }
        else:
            self.logger.error('Usage: qiwi_transaction_info\n'
                              'Failed to do request. status code:' + str(
                response.status_code) + '\n response text: ' + response.text)
            return False

    def qiwi_update_transactions(self):
        new_transactions = self.qiwi_payments(50, 'IN')
        for transaction in new_transactions:
            try:
                self.transactions.index(transaction)
            except ValueError:
                self.transactions.append(transaction)
                self.logger.debug('append new qiwi transaction to pool: ' + str(transaction))

    def qiwi_find_transaction(self, comment):
        """
        Find transaction IN TRANSACTION LIST, not from qiwi_api
        :return: <None/tuple> False if not found, tuple with transaction if found
        """
        self.qiwi_update_transactions()
        for transaction in self.transactions:
            if str(transaction['comment']) == str(comment):
                return transaction
        return None

    def qiwi_send_money(self, to_number, count, comment=' '):
        """

        :param to_number: <str> receiver's number (like 7800252123 (without '+',etc.), IF RUSSIA START FROM 7 ONLY)
        :param count: <float> sum of transaction (in RUB)
        :param comment: <str>
        :return: <tuple/None>tuple if success
        :raise <QiwiException> if qiwi returned error code
        """
        to_number = str(to_number)
        if to_number[0] == '+':
            self.logger.debug('Usage: qiwi_send_money\n'
                              'WRONG NUMBER FORMAT ' + to_number)
            to_number = to_number[1:]
        if to_number[0] == '8':
            self.logger.debug('Usage: qiwi_send_money\n'
                              'RUS NUMBER SHOULD START FROM 7, NOT 8 ' + to_number)
            to_number = '7' + to_number[1:]
        url = "https://edge.qiwi.com/sinap/api/v2/terms/99/payments"
        data = {
            'id': str(int(time.time()) + 1),
            'sum': {
                'amount': count,
                'currency': self.qiwi_curency['rub']
            },
            'paymentMethod': {
                'type': 'Account',
                'accountId': '643',
            },
            'fields': {
                'account': '+' + to_number,
                'comment': comment
            }
        }

        response = requests.post(url, json.dumps(data), headers=self.qiwi_headers)
        if response.status_code == 200:
            jresp = response.json()
            return {'txnId': jresp['transaction']['id'],
                    'status': jresp['transaction']['state']['code'],
                    'sum': jresp['sum']['amount'],
                    'receiver': jresp['fields']['account']}
        else:
            self.logger.error("Qiwi failed transaction status code : "+str(response.status_code)+'\nresponse data:\n'+response.text)
            raise self.QiwiException

    @staticmethod
    def logger_init(filename, name, level="DEBUG"):
        logger = logging.getLogger(name)
        if level == "INFO":
            logger.setLevel(logging.INFO)
        elif level == "DEBUG":
            logger.setLevel(logging.DEBUG)
        elif level == "ERROR":
            logger.setLevel(logging.ERROR)

        # create the logging file handler
        fh = logging.FileHandler(filename)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        # add handler to logger object
        logger.addHandler(fh)

        return logger
