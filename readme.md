Бот для проведения сделок гарантийных сделок по рекламе на каналах в telegram.
Кошелёк бота удерживает сумму, до момента выполнения условий по сделке

Для полноценного использования на сервере необходимо:
	1. Создать ключ и сертификат (по факту приватный и публичный RSA ключи) с помощью следующих команд
		openssl genrsa -out webhook_pkey.pem 2048
		openssl req -new -x509 -days 3650 -key webhook_pkey.pem -out webhook_cert.pem
	2. Создать файл config.py с классом Config, имеющий следующую структуру
		class Config:

			token = ""
			qiwi_token = ""
			qiwi_number = "79991234567"
			webhook_port = '8443' # 80,443,8443,8080
			webhook_ip = "192.192.123.1"
			ssl_cert = './webhook_cert.pem' # путь до публичного ключа 
			ssl_pom = './webhook_pkey.pem' # путь до секретного ключа